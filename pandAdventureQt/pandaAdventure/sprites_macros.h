#ifndef PANDAPandaSprites_H
#define PANDAPandaSprites_H

#define PandaStandby QPixmap(":PandaSprites/Panda_STANDY.png")

#define PandaUp0 QPixmap(":PandaSprites/Panda_UP0.png")
#define PandaUp1 QPixmap(":PandaSprites/Panda_UP1.png")
#define PandaUp2 QPixmap(":PandaSprites/Panda_UP2.png")
#define PandaUp3 QPixmap(":PandaSprites/Panda_UP3.png")
#define PandaUp4 QPixmap(":PandaSprites/Panda_UP4.png")


#define PandaRight0 QPixmap(":PandaSprites/Panda_LEFT0.png")
#define PandaRight1 QPixmap(":PandaSprites/Panda_LEFT1.png")
#define PandaRight2 QPixmap(":PandaSprites/Panda_LEFT2.png")
#define PandaRight3 QPixmap(":PandaSprites/Panda_LEFT3.png")
#define PandaRight4 QPixmap(":PandaSprites/Panda_LEFT4.png")

#define PandaLeft0 QPixmap(":PandaSprites/Panda_RIGHT0.png")
#define PandaLeft1 QPixmap(":PandaSprites/Panda_RIGHT1.png")
#define PandaLeft2 QPixmap(":PandaSprites/Panda_RIGHT2.png")
#define PandaLeft3 QPixmap(":PandaSprites/Panda_RIGHT3.png")
#define PandaLeft4 QPixmap(":PandaSprites/Panda_RIGHT4.png")

#define PandaUpLeft0 QPixmap(":PandaSprites/Panda_UPRIGHT0.png")
#define PandaUpLeft1 QPixmap(":PandaSprites/Panda_UPRIGHT1.png")
#define PandaUpLeft2 QPixmap(":PandaSprites/Panda_UPRIGHT2.png")
#define PandaUpLeft3 QPixmap(":PandaSprites/Panda_UPRIGHT3.png")
#define PandaUpLeft4 QPixmap(":PandaSprites/Panda_UPRIGHT4.png")

#define PandaUpRight0 QPixmap(":PandaSprites/Panda_UPLEFT0.png")
#define PandaUpRight1 QPixmap(":PandaSprites/Panda_UPLEFT1.png")
#define PandaUpRight2 QPixmap(":PandaSprites/Panda_UPLEFT2.png")
#define PandaUpRight3 QPixmap(":PandaSprites/Panda_UPLEFT3.png")
#define PandaUpRight4 QPixmap(":PandaSprites/Panda_UPLEFT4.png")

#define Asteroid1_photo QPixmap(":PandaSprites/cometa_halley.png")
#define Asteroid2_photo QPixmap(":PandaSprites/cometa_showmarker.png")
#define Asteroid3_photo QPixmap(":PandaSprites/cometa_hallebopp.png")

#define AsteroidRed1 QPixmap(":Asteroids/red1.png")
#define AsteroidRed2 QPixmap(":Asteroids/red2.png")
#define AsteroidRed3 QPixmap(":Asteroids/red3.png")
#define AsteroidBeige1 QPixmap(":Asteroids/beige1.png")
#define AsteroidBeige2 QPixmap(":Asteroids/beige2.png")
#define AsteroidBeige3 QPixmap(":Asteroids/beige3.png")
#define AsteroidBlue1 QPixmap(":Asteroids/blue1.png")
#define AsteroidBlue2 QPixmap(":Asteroids/blue2.png")
#define AsteroidBlue3 QPixmap(":Asteroids/blue3.png")
#define AsteroidBrown1 QPixmap(":Asteroids/brown1.png")
#define AsteroidBrown2 QPixmap(":Asteroids/brown2.png")
#define AsteroidBrown3 QPixmap(":Asteroids/brown3.png")
#define AsteroidBrown21 QPixmap(":Asteroids/brown_21.png")
#define AsteroidBrown22 QPixmap(":Asteroids/brown_22.png")
#define AsteroidBrown23 QPixmap(":Asteroids/brown_23.png")
#define BackgroundIntro QPixmap(":Backgrounds/intro.png")
#define BackgroundCredits QPixmap(":Backgrounds/credits.jpg")
#define BackgroundLeve1 QPixmap(":Backgrounds/level1.png")
#define BackgroundLeve2 QPixmap(":Backgrounds/level2.png")
#define BackgroundLeve3 QPixmap(":Backgrounds/level3.png")
#define BackgroundLeve4 QPixmap(":Backgrounds/level4.jpg")

#define logoNormal QPixmap(":logos/logo_normal.png")
#define logoLarge  QPixmap(":logos/logo_large.png")
#define logoSmall  QPixmap(":logos/logo_small.png")

#define logoLevel1 QPixmap(":logos/logo_level1.png")
#define logoLevel2 QPixmap(":logos/logo_level2.png")
#define logoLevel3 QPixmap(":logos/logo_level3.png")
#define logoLevel4 QPixmap(":logos/logo_level4.png")

#define logoPause  QPixmap(":logos/logo_pause.png")
#define logoPressStart QPixmap(":logos/logo_press_start.png")
#define logoCredits QPixmap(":logos/logo_creditos.png")

#define Control_Buttons QPixmap(":logos/control_buttons.PNG")
#define Control_Pots QPixmap(":logos/control_pots.PNG")

#endif // PANDAPandaSprites_H
