#ifndef SOUNDS_MACROS_H
#define SOUNDS_MACROS_H

#endif // SOUNDS_MACROS_H

#define  SOUND_INTRO    QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/intro.mp3")
#define  SOUND_OUTRO    QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/outro.mp3")
#define  SOUND_LEVEL1   QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/level1.mp3")
#define  SOUND_LEVEL2   QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/level2.mp3")
#define  SOUND_LEVEL3   QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/level3.mp3")
#define  SOUND_LEVEL4   QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/level4.mp3")

#define  SOUND_PANDACOIN        QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/clip_sounds/power_up.wav")
#define  SOUND_COIN             QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/clip_sounds/get_coin.ogg")
#define  SOUND_ASTEROID_COLLIDE QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/clip_sounds/explotion.mp3")


#define  SOUND_PAUSE    QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/clip_sounds/pause.mp3")
#define  SOUND_START    QUrl::fromLocalFile("/home/smart/Documents/QT_info_II/pandAdventure/pandAdventureSounds/clip_sounds/start.mp3")

