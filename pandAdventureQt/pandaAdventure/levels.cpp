#include "pandadventure.h"
#include <QWidget>
#include <QListView>
#include <QTextBrowser>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QTextItem>

//Methods to charge each Scene of the game


void pandAdventure::LoadIntro(){
    /*CLEAR VARIABLES*/
    scene->clear();

    while(mAsteroids.size() > 0){
        delete mAsteroids[0];
        mAsteroids.remove(0);
    }

    game_state = GAME_INTRO;
    backgroundManager(INTRO);

    /*ADD BANNERS*/
    sceneLogoPandAdventure = scene->addPixmap(logoLarge);
    sceneLogoPause =         scene->addPixmap(logoPressStart);
    sceneLogoPandAdventure->setPos((desktop_size.rwidth()/2 - 200),desktop_size.rheight()/2 - 90);
    sceneLogoPause->setPos(desktop_size.rwidth()/2 - 320,desktop_size.rheight()/2 + 90);
    arduinoSerialSender("LOCKED\r\n");  //Lock control before start the game
}

void pandAdventure::LoadLevel(){
    /*CLEAR VARIABLES*/
    scene->removeItem(sceneLogoPandAdventure);
    scene->removeItem(sceneLogoPause);
    if(sceneLogoControl != nullptr){
        scene->removeItem(sceneLogoControl);
    }
    if(showingCredits){
        hideCredits();
    }

    while(mAsteroids.size() > 0){
        scene->removeItem(mAsteroids[0]);
        delete mAsteroids[0];
        mAsteroids.remove(0);
    }
    scene->removeItem(mPanda);

    /*SET UP GAME SETTINGS*/
    game_state = GAME_INIT;
    emit(gameStart());
    backgroundManager(LEVEL);

    /*ASTEROIDS*/
    /*1*/mAsteroids.append(new asteroid(BLUE,LARGE));    //300x300
    /*2*/mAsteroids.append(new asteroid(BROWN1,LARGE));
    /*3*/mAsteroids.append(new asteroid(BEIGE,LARGE));

    mAsteroids[0]->setPos(200,100);
    mAsteroids[1]->setPos(200,desktop_size.rheight()-400);
    mAsteroids[2]->setPos(desktop_size.rwidth()-400,(desktop_size.rheight()/2)-150);

    for(int i = 0; i< mAsteroids.size();i++){
        scene->addItem(mAsteroids[i]);
    }

    /*PANDA*/
    mPanda->setPos(desktop_size.rwidth()/2,desktop_size.rheight()/2);
    scene->addItem(mPanda);

    /*SET BANNERS*/
    sceneLogoPandAdventure=scene->addPixmap(logoSmall);
    sceneLogoPandAdventure->setPos(logoPandaPoint);
    sceneLogoControl = scene->addPixmap(Control_Buttons);
    sceneLogoControl->setPos(0,desktop_size.rheight()-70);
    scene->addItem(sceneLogoControl);

    counter_colisions_asteroid[0] = 0;
    ui->lcdCounter0->display(counter_colisions_asteroid[0]);
    counter_colisions_asteroid[1] = 0;
    ui->lcdCounter1->display(counter_colisions_asteroid[1]);
    counter_colisions_asteroid[2] = 0;
    ui->lcdCounter2->display(counter_colisions_asteroid[2]);
    arduinoSerialSender("BUTTONS\r\n");

}

void pandAdventure::LoadCredits(){
    /*CLEAR VARIABLES*/
    scene->removeItem(sceneLogoPandAdventure);
    scene->removeItem(sceneLogoPause);
    while(mAsteroids.size() > 0){
        delete mAsteroids[0];
        mAsteroids.remove(0);
    }

    game_state = GAME_OUTRO;
    backgroundManager(CREDITS);

    showCredits();
}

void pandAdventure::showCredits(){
    showingCredits = true;
    /*SET BANNERS*/
    sceneLogoPandAdventure = scene->addPixmap(logoLarge);
    sceneLogoPandAdventure->setPos((desktop_size.rwidth()/2)-230,0);
    sceneCreditsLogo = scene->addPixmap(logoCredits);
    sceneCreditsLogo->setPos((desktop_size.rwidth()/2) -130 ,180);


    text_visualizer = new QGraphicsTextItem("");
    text_visualizer->setPos((desktop_size.rwidth()/2)-200,250);
    text_visualizer->setTextWidth(600);

    text_visualizer->setPlainText(
                    "INSTRUCTIONS:\n"
                    "1-Fly to the Asteroids to see information\n"
                    "2-Use the button 'Mode' in the control to select the control Mode:\n"
                    "      Buttons\n"
                    "      Potenciometers\n"
                    "3-In the Mode Buttons:\n"
                    "3.1-Use the numeric arrows to drive the aircraft: \n"
                    "      UP->Key 8\n"
                    "      DOWN->Key 2\n"
                    "      LEFT->Key 4\n"
                    "      RIGHT->Key 6\n"
                    "3.2-Use physical buttons in the control\n"
                    "4-In the Mode Potenciometers:\n"
                    "Use the potenciometers allocated in the control to drive the aircraft\n"
                    "5-Press 'Space' in any moment to reset the game\n"

                    "\nCREDITS:\n"
                    "Directed by\n"
                    "David Alejandro Rondon\n"
                    "Designed by\n"
                    "David Alejandro Rondon\n"
                    "Produced by\n"
                    "David Alejandro Rondon\n"
                    "Acknowledgements to\n"
                    "www.google.com\n"
                    "Alfa Testers:\n"
                    "David Alejandro Rondon\n"
                    "Beta Testers:\n"
                    "David Alejandro Rondon\n"

                    "\nPRESS 'Space' TO PLAY THE GAME\n"


                    );


    QFont fuente("Fantasy", 10, QFont::Bold);
    text_visualizer->setFont(fuente);
    scene->addItem(text_visualizer);
}
void pandAdventure::hideCredits(){
    showingCredits = false;
    scene->removeItem(sceneCreditsLogo);
    scene->removeItem(text_visualizer);
}

void pandAdventure::backgroundManager(typeScene type){
    switch (type) {
    case INTRO:
        ui->viewPrincipal->setBackgroundBrush(BackgroundIntro.scaled(desktop_size,Qt::IgnoreAspectRatio));
        break;
    case CREDITS:
        ui->viewPrincipal->setBackgroundBrush(BackgroundCredits.scaled(desktop_size,Qt::IgnoreAspectRatio));
        break;
    case LEVEL:
        ui->viewPrincipal->setBackgroundBrush(BackgroundLeve1.scaled(desktop_size,Qt::IgnoreAspectRatio));
        break;
    default:
        break;
    }
}



