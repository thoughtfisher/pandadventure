#-------------------------------------------------
#
# Project created by QtCreator 2015-01-31T22:53:05
#
#-------------------------------------------------

QT       += core gui serialport
QT       += multimedia
QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pandaAdventure
TEMPLATE = app


SOURCES += main.cpp\
        pandadventure.cpp \
        panda.cpp \
        asteroid.cpp \
        levels.cpp \
        asteroidsinformation.cpp \
        serialportmanager.cpp

HEADERS  += pandadventure.h \
    asteroid.h \
    sprites_macros.h \
    panda.h

FORMS    += pandadventure.ui

CONFIG += mobility
MOBILITY = 

OTHER_FILES +=

RESOURCES += \
    ../Sprites/pandAdventure.qrc

