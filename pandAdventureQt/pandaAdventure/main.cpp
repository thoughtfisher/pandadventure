#include "pandadventure.h"
#include <QApplication>
#include <QMediaPlayer>

//****************************************************DOCUMENTATION*************************************************//
//*// Filename:             RhemoPin.ino
//*// Description:          UI designed to implement a interactive visualizer
//*// Author:               David Alejandro Rondon Berrio (contact: blackphotografy@gmail.com, Cel: (+57)3016332408)
//*// Creation date:        11/03/2019, Medellín CO
//*// last modification:    11/03/2019, Medellín CO

/*pandAdventure is an desktop app which allows to visualize information in an interactive way.
Related videos :
https://www.youtube.com/watch?v=oYkylhjCXY4
https://www.youtube.com/watch?v=GDSVs5tSr2Q*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    pandAdventure w;
    w.show();
    w.showMaximized();

    return a.exec();
}
