#include "asteroid.h"


asteroid::asteroid(type_asteroid typeAsteroid, size sizeAsteroid)
{
      setType(typeAsteroid,sizeAsteroid);
}

void asteroid::setType(type_asteroid typeAsteroid, size sizeAsteroid){
    switch (typeAsteroid) {
    case BLUE:
        switch (sizeAsteroid) {
        case SMALL:
            setPixmap(QPixmap(AsteroidBlue1));
            break;
        case NORMAL:
            setPixmap(QPixmap(AsteroidBlue2));
            break;
        case LARGE:
            setPixmap(QPixmap(AsteroidBlue3));
            break;
        default:
            break;
        }
        break;
    case BEIGE:
        switch (sizeAsteroid) {
        case SMALL:
            setPixmap(QPixmap(AsteroidBeige1));
            break;
        case NORMAL:
            setPixmap(QPixmap(AsteroidBeige2));
            break;
        case LARGE:
            setPixmap(QPixmap(AsteroidBeige3));
            break;
        default:
            break;
        }
        break;
    case BROWN1:
        switch (sizeAsteroid) {
        case SMALL:
            setPixmap(QPixmap(AsteroidBrown1));
            break;
        case NORMAL:
            setPixmap(QPixmap(AsteroidBrown2));
            break;
        case LARGE:
            setPixmap(QPixmap(AsteroidBrown3));
            break;
        default:
            break;
        }
        break;
    case BROWN2:
        switch (sizeAsteroid) {
        case SMALL:
            setPixmap(QPixmap(AsteroidBrown21));
            break;
        case NORMAL:
            setPixmap(QPixmap(AsteroidBrown22));
            break;
        case LARGE:
            setPixmap(QPixmap(AsteroidBrown23));
            break;
        default:
            break;
        }
        break;
    case RED:
        switch (sizeAsteroid) {
        case SMALL:
            setPixmap(QPixmap(AsteroidRed1));
            break;
        case NORMAL:
            setPixmap(QPixmap(AsteroidRed2));
            break;
        case LARGE:
            setPixmap(QPixmap(AsteroidRed3));
            break;
        default:
            break;
        }
        break;

    default:
        break;
    }
}
