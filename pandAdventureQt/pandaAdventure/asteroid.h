#ifndef ASTEROID_H
#define ASTEROID_H

#include <QGraphicsPixmapItem>
#include <QMediaPlayer>
#include "sprites_macros.h"
enum size{SMALL,NORMAL,LARGE};
enum type_asteroid{BEIGE,RED,BLUE,BROWN1,BROWN2};

class asteroid : public QGraphicsPixmapItem
{
public:
    asteroid(type_asteroid typeAsteroid = BLUE, size sizeAsteroid= NORMAL);

public slots:
    void setType(type_asteroid typeAsteroid = BLUE, size sizeAsteroid= NORMAL);
private:


};

#endif // ASTEROID_H
