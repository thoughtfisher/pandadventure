#include "pandadventure.h"
#include <QWidgetItem>

/*------------------------------------CONSTRUCTOR-------------------------------------------*/
pandAdventure::pandAdventure(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::pandAdventure),  scene(new QGraphicsScene(this))//inicializo punteros
{
   /*SERIAL PORT SETTINGS*/
   initSerialPort();

   /*TIMER CONFIGURATION*/
   ppalTimer = new QTimer();
   millisToFrame = MILLIS_TO_FRAME;

   /*SCREEN CONFIGURATIONS*/
   //Getting dimensions
   QRect desktop_rec = QApplication::desktop()->screenGeometry();
   int desktop_height = desktop_rec.height();
   int desktop_width =  desktop_rec.width();

   //Setting Scenario
   ui->setupUi(this);
   ui->viewPrincipal->setGeometry(desktop_rec);
   ui->viewPrincipal->setScene(scene);
   scene->setSceneRect(0, 0, desktop_width, desktop_height);
   ui->viewPrincipal->setRenderHint(QPainter::Antialiasing);

   //Getting special measures
   desktop_size  = ui->viewPrincipal->size();
   logoPandaPoint.setX(desktop_width - 350);
   logoPandaPoint.setY(desktop_height - 160);


   /*CONNECTIONS*/
   connect(arduino_serial_port,SIGNAL(readyRead()),this,SLOT(arduinoSerialReceiver()));
   connect(ppalTimer,SIGNAL(timeout()),this,SLOT(timeTick()));
   connect(this,SIGNAL(gameStart()),this,SLOT(gameStart_ToDo()));
   QTimer::singleShot(0, this, SLOT(showFullScreen()));

   //Creating instance of the protagonis
   mPanda = new panda();

   /*INITAL VALUES/SETTINGS*/
   sceneLogoControl = nullptr;
   loadInitialValues();
   LoadIntro();
 }

/*-------------------------------------DESTRUCTOR------------------------------------------*/
pandAdventure::~pandAdventure()
{
    delete ui;
}
/*-------------------------------------METHODS-----------------------------------------*/
void pandAdventure::loadInitialValues()
{
    /*SCENE SETTINGS*/
    int frictionForce =  1;
    int forceLeft =     -5;
    int forceUp =       -5;
    int forceDown =      5;
    int forceRight =     5;
    /*PANDA SETTINGS*/
    mPanda->setFrictionForce(frictionForce);
    mPanda->setForceLeft(forceLeft);
    mPanda->setForceUp(forceUp);
    mPanda->setForceRight(forceRight);
    mPanda->setForceDown(forceDown);
    qDebug() << "SETTINGS:\n"
                << "\tFriction: "<< frictionForce << endl
                << "\tForce Left: "<< forceLeft << endl
                << "\tForce Up: "<< forceUp << endl
                << "\tForce Up: "<< forceDown << endl
                << "\tForce Right: "<< forceRight << endl;
}
void pandAdventure::collisionAnalyzer(){
    static int selected_asteroid = -1;      //-1 wants to say: none asteroid selected
    bool colision_flag = false;             //false: No colision, true: In colision
    /*ASTEROID COLLIDES*/
        for(int i=0;i< mAsteroids.size();i++){          //Review if panda is colliding with any asteroid
            if(mPanda->collidesWithItem(mAsteroids[i])){
                colision_flag = true;                   //Raising flag
                if(selected_asteroid == -1){
                    qDebug() << "Colision";
                    selected_asteroid = i;
                    counter_colisions_asteroid[i]++;
                    //Updating collision counters
                    if(i==0){
                        ui->lcdCounter0->display(counter_colisions_asteroid[i]);
                    }else if(i==1){
                        ui->lcdCounter1->display(counter_colisions_asteroid[i]);
                    }else if(i==2){
                        ui->lcdCounter2->display(counter_colisions_asteroid[i]);
                    }
                    //Showing related information
                    showAsteroidInformation(i);
                }
            }
        }
        if(colision_flag == false && selected_asteroid != -1){
            qDebug() << "Out colision";
            selected_asteroid = -1;
            hideAsteroidInformation();
        }
        /*BOUNDARIES COLLISION*/
        if((mPanda->pos().x() > desktop_size.rwidth()) || (mPanda->pos().x() < -100) || (mPanda->pos().y() < -150) ||(mPanda->pos().y() > desktop_size.rheight())){
             mPanda->setPos(desktop_size.rwidth()/2,desktop_size.rheight()/2);
        }
}
/*-------------------------------------SLOTS--------------------------------------------*/
void pandAdventure::timeTick(){
    if(millisToFrame > 0){
        if(--millisToFrame == 0){
            millisToFrame = MILLIS_TO_FRAME;
                mPanda->updateFrame(); //to update the sprite
                scene->advance();
        }
    }
    mPanda->tickSignal(); //Communicate the signal to panda
    ui->lcdX->display(mPanda->pos().x());
    ui->lcdY->display(mPanda->pos().y());
    collisionAnalyzer();  //Call the collisionAnalyzer each tick time
}

void pandAdventure::gameStart_ToDo()
{
    if(game_state ==  GAME_INTRO){
        LoadLevel();
    }else if(game_state ==  GAME_INIT){
        qDebug() << "GAME STATE: RUNNING";
        game_state = GAME_START;
        ppalTimer->start(1);
    }else if(game_state ==  GAME_PAUSE){
        game_state = GAME_START;
        ppalTimer->start(1);
    }

}


/*--------------------------------EVENTS--------------------------------------------*/
void pandAdventure::keyPressEvent(QKeyEvent *event){
    int key = event->key();
    switch (key) {
    case Qt::Key_8:
        if(control_mode == MODE_BUTTONS && game_state == GAME_START){
          qDebug() << "UP";
          mPanda->setDirection(UP);
        }
        break;
    case Qt::Key_2:
        if(control_mode == MODE_BUTTONS && game_state == GAME_START){
          qDebug() << "DOWN";
          mPanda->setDirection(DOWN);
        }
        break;
    case Qt::Key_4:
        if(control_mode == MODE_BUTTONS && game_state == GAME_START){
          qDebug() << "LEFT";
          mPanda->setDirection(LEFT);
        }
        break;
    case Qt::Key_6:
        if(control_mode == MODE_BUTTONS && game_state == GAME_START){
          qDebug() << "RIGHT";
          mPanda->setDirection(RIGHT);
        }
        break;
    case Qt::Key_Space:
        if(game_state == GAME_INIT || game_state == GAME_INTRO){
            emit(gameStart());
        }else if(game_state == GAME_START || game_state == GAME_OUTRO){
            LoadLevel();
        }
        break;
    case Qt::Key_C:
        if(game_state == GAME_INTRO){
            qDebug() << "C pressed";
            LoadCredits();
        }
        break;
    default:
        break;
    }
}

void pandAdventure::keyReleaseEvent(QKeyEvent *event){
    if(control_mode == MODE_BUTTONS && game_state == GAME_START){
        qDebug() << "RELEASED KEY";
        mPanda->setDirection(STANDBY);
    }
}

/*-----------------------------GETTERS AND SETTERS--------------------------------------------*/

QTimer *pandAdventure::getPpalTimer() const
{
    return ppalTimer;
}

void pandAdventure::setPpalTimer(QTimer *value)
{
    ppalTimer = value;

}


