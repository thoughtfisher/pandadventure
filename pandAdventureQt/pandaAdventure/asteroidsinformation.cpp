#include <pandadventure.h>

void pandAdventure::showAsteroidInformation(int asteroid_number)
{
    text_visualizer = new QGraphicsTextItem("");
    text_visualizer->setPos((desktop_size.width()/2)-200,550);
    text_visualizer->setTextWidth(500);

    if(asteroid_number == 0){
        photo_visualizer = new QGraphicsPixmapItem(Asteroid1_photo);
        text_visualizer->setPlainText("En 1705 el astrónomo inglés Edmond Halley, determinó que el cometa orbita alrededor del Sol cada 76 años. También predijo que volvería en 1758, hecho que sucedió, aunque él no pudo comprobarlo al haber fallecido en 1742."
                                           "\nEs gracias a Halley que sabemos que los cometas son astros que orbitan alrededor del Sol. Otra de las particularidades de este astro es que es visible a simple vista desde la Tierra. La última vez que fue visto fue en 1986 y está previsto que vuelva a hacerlo en 2061"
                        );
    }else if(asteroid_number == 1){
        photo_visualizer = new QGraphicsPixmapItem(Asteroid2_photo);
        text_visualizer->setPlainText("En julio de 1994, el cometa Shoemaker-Levy 9 (SL9) impactó en Júpiter y dejó gigantescas cicatrices oscuras en la atmósfera del planeta. El cometa ya había sido descubierto por los astrónomos David Levy y Eugene M. Shoemaker cuando orbitaba Júpiter."
                                           "\nAl descubrirse que el cometa impactaría contra el planeta, los científicos y los medios de comunicación de todo el mundo siguieron la colisión en directo. La cual se convirtió en la primera observación de un choque extraterrestre en el Sistema Solar."
                        );
    }else if(asteroid_number == 2){
        photo_visualizer = new QGraphicsPixmapItem(Asteroid3_photo);
        text_visualizer->setPlainText("El cometa Hale-Bopp, era cuatro veces más grande que Halley y es el último que ha podido ser observado por los aficionados a simple vista. Fue descubierto el 23 de julio de 1995 por Alan Hale, un científico de la NASA y astrónomo aficionado, y por Thomas Bopp, un aficionado puro."
                                           "\nExiste una historia macabra que envuelve el paso de este cometa. Marshall Applewhite, líder de la sectaHeaven´s Gate en Rancho Santa Fe, California, logró convencer a 38 de sus seguidores de que tenían que suicidarse para que sus almas se eleven a la nave espacial que se encontraba escondida detrás del cometa.Los integrantes de esta secta creían que eran extraterrestres. Los hombres de la secta se dejaron castrar y en vísperas de su suicidio, bebieron zumo de limón a fin de purificar su cuerpo."
                        );
    }

    photo_visualizer->setPos((desktop_size.width()/2)-200,200);
    QFont fuente("Fantasy", 10, QFont::Bold);
    text_visualizer->setFont(fuente);
    scene->addItem(text_visualizer);
    scene->addItem(photo_visualizer);
}

void pandAdventure::hideAsteroidInformation()
{
    scene->removeItem(text_visualizer);
    scene->removeItem(photo_visualizer);
    text_visualizer=NULL;
    text_visualizer=NULL;
}
