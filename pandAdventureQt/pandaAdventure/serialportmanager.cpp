#include <pandadventure.h>

//Methods to control the serial port and process the information received through it

void pandAdventure::initSerialPort()
{
    /*LINUX*/// arduino_serial_port = new QSerialPort("ttyUSB0");
    /*WINDOWS*/ arduino_serial_port = new QSerialPort(SERIAL_PORT);
     arduino_serial_port->setBaudRate(SERIAL_BAUD_RATE);
     arduino_serial_port->setDataBits(QSerialPort::Data8);
     arduino_serial_port->setParity(QSerialPort::NoParity);
     arduino_serial_port->setStopBits(QSerialPort::OneStop);
     if(arduino_serial_port->open(QIODevice::ReadWrite) == true){
         qDebug() << "Connection succesfull";
     }else{
         qDebug() << "Connection fail";
     }
}
void pandAdventure::arduinoSerialReceiver()
{
    QString received = arduino_serial_port->readAll();
    received_from_arduino.append(received);
    if(received_from_arduino.indexOf("\r\n") != -1){        //Looking for ending frame
        processString(received_from_arduino);
        received_from_arduino.clear();
    }
}

void pandAdventure::arduinoSerialSender(QString value){
    arduino_serial_port->write(value.toLocal8Bit());
    arduino_serial_port->flush();
}

void pandAdventure::processString(QString string_received){
    qDebug() << "String received" << string_received  << " Length " << string_received.length();
    if(string_received == "M_BUTTONS\r\n"){
        qDebug() << "Arduino control: M_BUTTONS"<<endl;

        control_mode = MODE_BUTTONS;
        sceneLogoControl->setPixmap(Control_Buttons);
    }else if(string_received == "M_POTS\r\n"){
        qDebug() <<  "Arduino control: M_POTS"<<endl;
        control_mode = MODE_POTS;
        sceneLogoControl->setPixmap(Control_Pots);
    }else{
        if(control_mode == MODE_BUTTONS){
            if(string_received == "UP\r\n"){
                qDebug() << "Arduino control: UP"<<endl;
                mPanda->setDirection(UP);
            }else if(string_received == "LEFT\r\n"){
                qDebug() << "Arduino control: LEFT"<<endl;
                mPanda->setDirection(LEFT);
            }else if(string_received == "RIGHT\r\n"){
                qDebug() << "Arduino control: RIGHT"<<endl;
                mPanda->setDirection(RIGHT);
            }else if(string_received == "DOWN\r\n"){
                qDebug() << "Arduino control: DOWN"<<endl;
                mPanda->setDirection(DOWN);
            }else if(string_received == "STANDBY\r\n"){
                qDebug() << "Arduino control: STANDBY"<<endl;
                mPanda->setDirection(STANDBY);
            }else{
                qDebug() << "Arduino control: UNKNOWN"<<endl;
            }
        }else if(control_mode == MODE_POTS){
            if(string_received.length() == 12){
                if(string_received[0] == 'X' && string_received[5] == 'Y'){
                    QStringRef xString(&string_received, 1, 4); //Taking 4 chars: X coordinate
                    qDebug() << "X "<< xString;
                    QStringRef yString(&string_received, 6, 4); //Taking 4 chars: Y coordinate
                    qDebug() << "Y "<< yString <<endl;
                    mPanda->setPos((xString.toInt()*(desktop_size.rwidth()-100))/1023,(desktop_size.rheight()-100)-(yString.toInt()*(desktop_size.rheight()-100))/1023);
                }else{
                    qDebug() << "Arduino control: FORMAT ERROR"<<endl;
                }
            }else{
                qDebug() << "Arduino control: LENGHT ERROR"<<endl;
            }
        }
    }
}
