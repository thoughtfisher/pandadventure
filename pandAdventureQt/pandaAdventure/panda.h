#ifndef PANDA_H
#define PANDA_H

#include <QGraphicsPixmapItem>
#include "sprites_macros.h"
#include <QtMath>
#include <QDebug>


#define MILLIS_TO_UPDATEPOS_STATIC  0
#define MILLIS_TO_UPDATEPOS_SLOW    64
#define MILLIS_TO_UPDATEPOS_NORMAL  48
#define MILLIS_TO_UPDATEPOS_FAST    32
#define MILLIS_TO_UPDATEPOS_ULTRAFAST 16

#define FINAL_DELTA_Y 20        //MAX Acceleration - AxisY
#define FINAL_DELTA_X 20        //MAX Acceleration - AxisX

enum Direction{STANDBY,UP,DOWN,LEFT,RIGHT};
class panda : public QGraphicsPixmapItem
{
private:

    int counterFrame;                               //Used to indicate the frame to be charged
    int TICKSTOUPDATEPOS;
    int ticksToUpdatePos;
    int forceUp,forceLeft,forceRight, forceDown;    //Variables to indicate the force of the aircraft in each direction
    int frictionForce;
    int deltaX,deltaY;                              //Variables to store the step in each axis
    Direction direction;

public:
    panda();
    void setPandaImage(const QPixmap &value);
    void reset();
    void updateFrame();                             //Function to update the panda sprite
    void tickSignal();                              //Function to process the game tick signal
    void changePosition();                          //Set the panda in the position calculated with the next functions
    int newPositionX();                             //Used to calculate the new position in Axis X
    int newPositionY();                             //Used to calculate the new position in Axis Y

    Direction getDirection() const;
    void setDirection(const Direction &value);
    int getForceUp() const;
    void setForceUp(int value);
    int getForceLeft() const;
    void setForceLeft(int value);
    int getForceRight() const;
    void setForceRight(int value);
    int getFrictionForce() const;
    void setFrictionForce(int value);
    int getDeltaX() const;
    void setDeltaX(int value);
    int getDeltaY() const;
    void setDeltaY(int value);
    int getForceDown() const;
    void setForceDown(int value);
};

#endif // PANDA_H
