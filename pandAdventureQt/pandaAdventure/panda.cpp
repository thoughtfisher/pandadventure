#include "panda.h"
#include <QMovie>
panda::panda(){
   reset();
}

void panda::reset(){    //Initialize each panda values
    deltaX = 0;
    deltaY = 0;
    counterFrame = 0;
    forceUp = 0;
    forceLeft = 0;
    forceRight = 0;
    frictionForce = 0;
    direction = STANDBY;
    TICKSTOUPDATEPOS = MILLIS_TO_UPDATEPOS_NORMAL;
    ticksToUpdatePos = TICKSTOUPDATEPOS;
    setPixmap(QPixmap(":PandaSprites/Panda_STANDY.png"));
}

void panda::changePosition(){
    setPos(newPositionX(),newPositionY());
}

int panda::newPositionX(){
    //GAME CINEMATICS
    int totalForceX;
    if(direction == LEFT){
         totalForceX = forceLeft;
    }else if(direction == RIGHT){
        totalForceX = forceRight;
    }else {
        // If these key are not pressed we verify the movement to add the friction force
        if(deltaX >0){
            totalForceX = (-1)* frictionForce;
            if(deltaX + totalForceX<0){
                totalForceX = 0;            //is neccesary to verify this when the forces are uneven and the
                                            //friction is even
            }
        }else if(deltaX<0){
            totalForceX =  (1)*frictionForce;
            if(deltaX + totalForceX>0){
                totalForceX = 0;
            }
        }else{
            totalForceX = 0;
        }
    }

    if((deltaX + totalForceX <= FINAL_DELTA_X) &&  (deltaX + totalForceX >= ((-1)*FINAL_DELTA_X))){
        deltaX += totalForceX;
    }
    return (pos().x() + deltaX) ;

}

int panda::newPositionY()
{
    //GAME CINEMATICS
    int totalForceY;
    if(direction == UP){
        totalForceY = forceUp;
    }else if(direction == DOWN){
        totalForceY = forceDown;
    }else {
        // If these key are not pressed we verify the movement to add the friction force
        if(deltaY >0){
            totalForceY = (-1)* frictionForce;
            if(deltaY + totalForceY<0){
                totalForceY = 0;            //is neccesary to verify this when the forces are uneven and the
                                            //friction is even
            }
        }else if(deltaY<0){
            totalForceY =  (1)*frictionForce;
            if(deltaY + totalForceY>0){
                totalForceY = 0;
            }
        }else{
            totalForceY = 0;
        }
    }

    if((deltaY + totalForceY <= FINAL_DELTA_Y) &&  (deltaY + totalForceY >= ((-1)*FINAL_DELTA_Y))){
        deltaY += totalForceY;
    }
    return (pos().y() + deltaY) ;

}


void panda::updateFrame()
{
    switch (direction) {
    case UP:
        switch (counterFrame) {
        case 0:
            counterFrame++;
            setPixmap(PandaUp0);
            break;
        case 1:
            counterFrame++;
            setPixmap(PandaUp1);
            break;
        case 2:
            counterFrame++;
            setPixmap(PandaUp2);
            break;
        case 3:
            counterFrame++;
            setPixmap(PandaUp3);
            break;
        case 4:
            counterFrame=1;
            setPixmap(PandaUp4);
            break;
        default:
            break;
        }
        break;
    case LEFT:
        switch (counterFrame) {
        case 0:
            counterFrame++;
            setPixmap(PandaLeft0);
            break;
        case 1:
            counterFrame++;
            setPixmap(PandaLeft1);
            break;
        case 2:
            counterFrame++;
            setPixmap(PandaLeft2);
            break;
        case 3:
            counterFrame++;
            setPixmap(PandaLeft3);
            break;
        case 4:
            counterFrame=1;
            setPixmap(PandaLeft4);
            break;
        default:
            break;
        }
        break;


    case RIGHT:
        switch (counterFrame) {
        case 0:
            counterFrame++;
            setPixmap(PandaRight0);
            break;
        case 1:
            counterFrame++;
            setPixmap(PandaRight1);
            break;
        case 2:
            counterFrame++;
            setPixmap(PandaRight2);
            break;
        case 3:
            counterFrame++;
            setPixmap(PandaRight3);
            break;
        case 4:
            counterFrame=1;
            setPixmap(PandaRight4);
            break;
        default:
            break;
        }
        break;

    case STANDBY:
        switch (counterFrame) {
        case 0:
            counterFrame++;
            setPixmap(PandaUp0);
            break;
        case 1:
            counterFrame++;
            setPixmap(PandaUp1);
            break;
        case 2:
            counterFrame = 1;
            setPixmap(PandaUp2);
            break;
        default:
            break;
        }
        break;
    case DOWN:
         setPixmap(PandaStandby);
        break;
    }
}

void panda::tickSignal(){
    if(ticksToUpdatePos > 0){
        if(--ticksToUpdatePos == 0){
            ticksToUpdatePos = TICKSTOUPDATEPOS;
            changePosition();
        }
    }
}



//****************************************************************************
Direction panda::getDirection() const
{
    return direction;
}

void panda::setDirection(const Direction &value)
{
    direction = value;
    counterFrame = 0;
}

int panda::getForceUp() const
{
    return forceUp;
}

void panda::setForceUp(int value)
{
    forceUp = value;
}
int panda::getForceLeft() const
{
    return forceLeft;
}

void panda::setForceLeft(int value)
{
    forceLeft = value;
}
int panda::getForceRight() const
{
    return forceRight;
}

void panda::setForceRight(int value)
{
    forceRight = value;
}
int panda::getFrictionForce() const
{
    return frictionForce;
}

void panda::setForceDown(int value)
{
    forceDown = value;
}
int panda::getForceDown() const
{
    return forceDown;
}
void panda::setFrictionForce(int value)
{
    frictionForce = value;
}
int panda::getDeltaX() const
{
    return deltaX;
}

void panda::setDeltaX(int value)
{
    deltaX = value;
}
int panda::getDeltaY() const
{
    return deltaY;
}

void panda::setDeltaY(int value)
{
    deltaY = value;
}










