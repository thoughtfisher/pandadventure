#ifndef PANDADVENTURE_H
#define PANDADVENTURE_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPathItem>
#include <QGraphicsWidget>
#include <QSerialPort>
#include <QMovie>
#include <stdlib.h>
#include <QSound>
#include <QDesktopWidget>

#include <QString>
#include <QKeyEvent>
#include <QDebug>
#include <QString>
#include <QTimer>
#include <QLabel>
#include <QMediaPlayer>

#include "panda.h"
#include "asteroid.h"
#include "ui_pandadventure.h"

#define MILLIS_TO_FRAME 16
#define SERIAL_PORT      "COM8"
#define SERIAL_BAUD_RATE QSerialPort::Baud9600
namespace Ui {
class pandAdventure;
}
enum controlMode{MODE_BUTTONS, MODE_POTS};                                          //Ways to control the game
enum typeScene{INTRO,CREDITS,LEVEL};                                                //Scenes of the game
enum gameStates{GAME_INTRO,GAME_INIT,GAME_PAUSE,GAME_START, GAME_OVER,GAME_OUTRO};  //States to control the game
class pandAdventure : public QMainWindow
{
    Q_OBJECT
private:
    gameStates game_state;                  //Variable to store the state of the came
    bool control_mode = MODE_BUTTONS;       //Indicate which control was selected
    bool showingCredits = false;            //Indicate if "Credits" are beeing showed
    QVector<asteroid*>  mAsteroids;         //Vector of asteroids objects
    int counter_colisions_asteroid[3];      //Colision counters

    QSerialPort *arduino_serial_port;       //Variable to access the serial port
    QString     received_from_arduino;      //Variable to store the received string through the serial port

    QTimer *ppalTimer;                      //Timer to produce the Tick signal
    int millisToFrame;

    Ui::pandAdventure *ui;                  //User interface object
    QGraphicsScene *scene;                  //Game Scene

    //Graphics items to deploy in the game scene
    panda *mPanda;
    QGraphicsTextItem   *text_visualizer;
    QGraphicsPixmapItem *photo_visualizer;
    QGraphicsPixmapItem* sceneLogoLevel,*sceneLogoPandAdventure,*sceneLogoPause, *sceneCreditsLogo, *sceneLogoControl;

    //Some important measures
    QPoint centralPoint;
    QPoint logoPandaPoint;
    QSize desktop_size;

public:
    explicit pandAdventure(QWidget *parent = 0);
    ~pandAdventure();

    QTimer *getPpalTimer() const;                   //Defined in pandadventure.cpp
    void setPpalTimer(QTimer *value);               //Defined in pandadventure.cpp
    void loadInitialValues();                       //Defined in pandadventure.cpp
    void backgroundManager(typeScene type=INTRO);   //Defined in pandadventure.cpp
    void collisionAnalyzer();                       //Defined in pandadventure.cpp
    void LoadIntro();                               //Defined in levels.cpp
    void LoadLevel();                               //Defined in levels.cpp
    void LoadCredits();                             //Defined in levels.cpp
    void showCredits();                             //Defined in levels.cpp
    void hideCredits();                             //Defined in levels.cpp
    void processString(QString );                   //Defined in serialportmanager.cpp

private slots:
    void  arduinoSerialReceiver();                  //Defined in serialportmanager.cpp
    void  arduinoSerialSender(QString);             //Defined in serialportmanager.cpp
    void  initSerialPort();                         //Defined in serialportmanager.cpp
    void  timeTick();                               //Defined in pandadventure.cpp
    void  gameStart_ToDo();                         //Defined in pandadventure.cpp
    void showAsteroidInformation(int);              //Defined in asteroidinformation.cpp
    void hideAsteroidInformation();                 //Defined in asteroidinformation.cpp

protected:
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *event);

signals:
    void  gameStart();
};

#endif // PANDADVENTURE_H
