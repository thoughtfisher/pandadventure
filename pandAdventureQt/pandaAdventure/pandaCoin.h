#ifndef PANDACOIN_H
#define PANDACOIN_H

#include <QGraphicsPixmapItem>
#include <QMediaPlayer>
#include "sounds_macros.h"
#include "sprites_macros.h"

class pandaCoin : public QGraphicsPixmapItem
{
public:
    pandaCoin();

signals:

public slots:
    static void pandaCoinGetted();
    static QMediaPlayer *pandaCoin_player ;
    static void loadSound();
private:



};

#endif // PANDACOINS_H
