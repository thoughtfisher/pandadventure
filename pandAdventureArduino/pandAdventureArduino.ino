//****************************************************DOCUMENTATION*************************************************//
//*// Filename:             RhemoPin.ino
//*// Description:          Firmware designed to implement an interactive control to pandAdventure Game
//*// Author:               David Alejandro Rondon Berrio (contact: blackphotografy@gmail.com, Cel: (+57)3016332408)
//*// Creation date:        11/03/2019, Medellín CO
//*// last modification:    11/03/2019, Medellín CO

/*pandAdventure is an desktop app which allows to visualize information in an interactive way.
Related videos :
https://www.youtube.com/watch?v=oYkylhjCXY4
https://www.youtube.com/watch?v=GDSVs5tSr2Q*/

//#define TESTING_PINS        //UNCOMMENT THIS LINE TO RUN A TEST OF THE PINS IN THE SERIAL MONITOR

/*********************Libraries*****************************/
#include "SerialStrRx.h"
/*********************Definitions***************************/
#define BAUD_RATE 9600
#define PIN_BUTTON_UP     5
#define PIN_BUTTON_DOWN   3
#define PIN_BUTTON_LEFT   6
#define PIN_BUTTON_RIGHT  4
#define PIN_BUTTON_MODE   2
#define PIN_POT_X         A1
#define PIN_POT_Y         A0
#define PIN_LED_RED       14
#define PIN_LED_GREEN     15
#define PIN_LED_BLUE      16
#define PIN_SSR_LIGHT     18

//Macros to control leds and lights
#define LED_GREEN_ON      digitalWrite(PIN_LED_GREEN, HIGH)
#define LED_GREEN_OFF     digitalWrite(PIN_LED_GREEN, LOW)
#define LED_BLUE_ON       digitalWrite(PIN_LED_BLUE, HIGH)
#define LED_BLUE_OFF      digitalWrite(PIN_LED_BLUE, LOW)
#define LED_RED_ON        digitalWrite(PIN_LED_BLUE, HIGH)
#define LED_RED_OFF       digitalWrite(PIN_LED_BLUE, LOW)
#define LIGHT_ON          digitalWrite(PIN_SSR_LIGHT, HIGH);
#define LIGHT_OFF         digitalWrite(PIN_SSR_LIGHT, LOW);

//Macros to select an operation mode
#define SELECT_MODE_BUTTONS   Serial.println("M_BUTTONS"); delay(100);   digitalWrite(PIN_LED_GREEN, HIGH);digitalWrite(PIN_LED_BLUE, LOW); control_mode = MODE_BUTTONS
#define SELECT_MODE_POTS      Serial.println("M_POTS");    delay(100);   digitalWrite(PIN_LED_GREEN, LOW); digitalWrite(PIN_LED_BLUE, HIGH);control_mode = MODE_POTS
#define SELECT_MODE_LOCKED    control_mode =  MODE_LOCKED

//Macros to send a serial command quickly
#define SEND_COMMAND_TO_UI(_COMMAND)  Serial.println(_COMMAND)
#define CMD_UP       "UP"
#define CMD_DOWN     "DOWN"
#define CMD_LEFT     "LEFT"
#define CMD_RIGHT    "RIGHT"
#define CMD_STANDBY  "STANDBY"
/*********************Global variables and objects**********/
SerialStrRx serial_str_rx(Serial);  //Object to receive information through the Serial port easily

//Definition of a new variable to store the current control mode
typedef enum Control_mode {MODE_LOCKED, MODE_BUTTONS, MODE_POTS};
//LOCKED is used to avoid to send information to the UI while it isn't ready to receive it.
//BUTTONS is used to send, only, the information produced by the buttons to the UI
//POTS is used to send, only, the information produced by the potenciometers to the UI
Control_mode control_mode = MODE_LOCKED;

//Needed variables to detect button events
int button_mode_state = 0;          // current state of the  button mode
int last_button_mode_state = 0;     // previous state of the button mode
int button_up_state = 0;            // current state of the  button up
int last_button_up_state = 0;       // previous state of the button up
int button_down_state = 0;          // current state of the  button down
int last_button_down_state = 0;     // previous state of the button down
int button_left_state = 0;          // current state of the  button left
int last_button_left_state = 0;     // previous state of the button left
int button_right_state = 0;         // current state of the  button right
int last_button_right_state = 0;    // previous state of the button right
char buffer[10] ;

void setup() {
  /********************Serial port init***********************/
  Serial.begin(BAUD_RATE);
  /********************SerialStrRx Settings*******************/
  serial_str_rx.selectStringTermination(STR_CR_LF);     //*Other string terminations are: STR_CR, STR_LF, STR_NULL or a custom string(p.e: "*", "END")
  serial_str_rx.attachCallback(processStringCallback);  //*Attach the function where the received strings will be processed by the user
  /********************Pin initialization  *******************/
  pinMode(PIN_BUTTON_UP, INPUT_PULLUP);
  pinMode(PIN_BUTTON_DOWN, INPUT_PULLUP);
  pinMode(PIN_BUTTON_LEFT, INPUT_PULLUP);
  pinMode(PIN_BUTTON_RIGHT, INPUT_PULLUP);
  pinMode(PIN_BUTTON_MODE, INPUT_PULLUP);
  pinMode(PIN_LED_GREEN, OUTPUT);
  pinMode(PIN_LED_BLUE, OUTPUT);
  pinMode(PIN_LED_RED, OUTPUT);
  pinMode(PIN_SSR_LIGHT, OUTPUT);
  //Set initial mode
  SELECT_MODE_LOCKED;
}

void loop() {
  #ifdef TESTING_PINS
  testingRutine();
  #else
  serial_str_rx.loop(); //*This function look for a string ending with the selected string termination into the serial rx buffer
  if (control_mode == MODE_LOCKED) { //While control is locked ... Blink Green<->Blue
    LED_BLUE_ON; LED_GREEN_OFF; delay(100);
    LED_BLUE_OFF; LED_GREEN_ON; delay(100);
  } else { //If control is unlocked...
    check_button_mode();  //function to detect a control mode change
    if (control_mode == MODE_BUTTONS) {
      mode_buttons_logic();
    } else if (control_mode == MODE_POTS) {
      mode_pots_logic();
    }
  }
  #endif
}

//Function to execute when the control mode is MODE_BUTTONS
void mode_buttons_logic() {
  check_button_up();
  check_button_down();
  check_button_left();
  check_button_right();
}
//Function to execute when the control mode is MODE_POTS
void mode_pots_logic() {
  String frame;
  sprintf(buffer, "%.4d", 1023 - analogRead(PIN_POT_X));  //Adjust the String to 4 chars
  frame = "X" + String(buffer);
  sprintf(buffer, "%.4d", 1023 - analogRead(PIN_POT_Y));  //Adjust the String to 4 chars
  frame += "Y" + String(buffer);
  Serial.println(frame);
  delay(16);
}
//function to detect a control mode change
void check_button_mode() {
  button_mode_state = digitalRead(PIN_BUTTON_MODE);
  if (button_mode_state != last_button_mode_state) {    // if the button state has changed...
    if (button_mode_state == LOW) { // if the current state is LOW then the button is pressed
      if (control_mode == MODE_BUTTONS) {
        SELECT_MODE_POTS;
      } else {
        SELECT_MODE_BUTTONS;
      }
    } else { // if the current state is HIGH then the button was released:
      //Dont Care
    }
  }
  last_button_mode_state = button_mode_state;
}
//function to detect a button UP events
void check_button_up() {
  button_up_state = digitalRead(PIN_BUTTON_UP);
  if (button_up_state != last_button_up_state) {    // if the button state has changed...
    if (button_up_state == LOW) { // if the current state is LOW then the button is pressed
      SEND_COMMAND_TO_UI(CMD_UP);
    } else { // if the current state is HIGH then the button was released:
      SEND_COMMAND_TO_UI(CMD_STANDBY);
    }
  }
  last_button_up_state = button_up_state;
}
//function to detect a button DOWN events
void check_button_down() {
  button_down_state = digitalRead(PIN_BUTTON_DOWN);
  if (button_down_state != last_button_down_state) {    // if the button state has changed...
    if (button_down_state == LOW) { // if the current state is LOW then the button is pressed
      SEND_COMMAND_TO_UI(CMD_DOWN);
    } else { // if the current state is HIGH then the button was released:
      SEND_COMMAND_TO_UI(CMD_STANDBY);
    }
  }
  last_button_down_state = button_down_state;
}
//function to detect a button LEFT events
void check_button_left() {
  button_left_state = digitalRead(PIN_BUTTON_LEFT);
  if (button_left_state != last_button_left_state) {    // if the button state has changed...
    if (button_left_state == LOW) { // if the current state is LOW then the button is pressed
      SEND_COMMAND_TO_UI(CMD_LEFT);
    } else { // if the current state is HIGH then the button was released:
      SEND_COMMAND_TO_UI(CMD_STANDBY);
    }
  }
  last_button_left_state = button_left_state;
}
//function to detect a button RIGHT events
void check_button_right() {
  button_right_state = digitalRead(PIN_BUTTON_RIGHT);
  if (button_right_state != last_button_right_state) {    // if the button state has changed...
    if (button_right_state == LOW) { // if the current state is LOW then the button is pressed
      SEND_COMMAND_TO_UI(CMD_RIGHT);
    } else { // if the current state is HIGH then the button was released:
      SEND_COMMAND_TO_UI(CMD_STANDBY);
    }
  }
  last_button_right_state = button_right_state;
}
//Callback to process string that comes through the serial port
void processStringCallback(String string_received) {    //*This function is called when a string ending with the selected string termination is found
  //Write 'To do' with string_received...For example:
  //Serial.println("String received: " + string_received);//Show the received string
  if(string_received.equals("BUTTONS")) {
    SELECT_MODE_BUTTONS;
  } else if (string_received.equals("POTS")) {
    SELECT_MODE_POTS;
  } else if (string_received.equals("LOCKED")) {
    SELECT_MODE_LOCKED;
  } else {
    Serial.println("Received command is known");
  }
}
#ifdef TESTING_PINS
void testingRutine() {
  static boolean led_selector = false;
  Serial.println("UP: " + String(digitalRead(PIN_BUTTON_UP)) +
                 " DOWN: " + String(digitalRead(PIN_BUTTON_DOWN)) +
                 " LEFT: " + String(digitalRead(PIN_BUTTON_LEFT)) +
                 " RIGHT: " + String(digitalRead(PIN_BUTTON_RIGHT)) +
                 " MODE: " + String(digitalRead(PIN_BUTTON_MODE)) +
                 " POT X: " + String(1023 - analogRead(PIN_POT_X)) +
                 " POT Y: " + String(1023 - analogRead(PIN_POT_Y)) +
                 " LED G: " + digitalRead(PIN_LED_GREEN) +
                 " LED B: " + digitalRead(PIN_LED_BLUE));
  if(led_selector) {
    digitalWrite(PIN_LED_GREEN, HIGH); digitalWrite(PIN_LED_BLUE, LOW);
  } else {
    digitalWrite(PIN_LED_GREEN, LOW); digitalWrite(PIN_LED_BLUE, HIGH);
  }
  led_selector = !led_selector;
  delay(200);
}
#endif

